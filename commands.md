# Commands
## Misc
Command | Description | Usage | Aliases | DMs | Permissions
--- | --- | --- | --- | --- | ---
`info` | See information about Antiowo | - | - | + | -
`invite` | Invite Antiowo to a server | - | `inv`, `link` | + | -
`kills` | See Antiowo's kill count | - | `bans`, `kicks` | + | -
`patrons` | See Antiowo's patrons | - | `patreon`, `patron`, `donators` | + | -
`support` | See the ways to support Antiowo | - | `donate` | + | -
`redeem` | Redeem an Antiowo Pass gift code | `redeem code` | - | + | -
`subscribe` | Subscribe to the vote reminder | - | `reminder`, `sub`, `remind`, `votereminder` | + | -
`unsubscribe` | Unsubscribe from the vote reminder | - | `unsub` | + | -
`vote` | Vote for Antiowo, see your vote count and Antiowo Passes | - | `votes`, `pass`, `passes`, `passcount` | + | -

## Mod
Command | Description | Usage | Aliases | DMs | Permissions
--- | --- | --- | --- | --- | ---
`blacklist` | Manage the word blacklist | `blacklist action [input]` | `bl`, `wordblacklist` | - | `BAN MEMBERS`
`delete` | Toggle message deletion after action | - | `deletion`, `dl` | - | `BAN MEMBERS`
`immunity` | Toggle Antiowo Passes | - | `im` | - | `BAN MEMBERS`
`nickname` | Toggle nickname scanning | - | `nick`, `nk` | - | `BAN MEMBERS`
`prefix` | Change the prefix in this server | `prefix [prefix]` | `setprefix`, `pr` | - | `MANAGE MESSAGES`
`punishment` | Manage the punishments | `punishment [type]` | `punish`, `pn` | - | `BAN MEMBERS`
`strictness` | Manage the strictness of owo/uwu detection | `strictness [type]` | `strict`, `st` | - | `BAN MEMBERS`
`warnings` | Manage warnings | `warnings [@member/number] [clear]` | `warning`, `warn`, `wr` | - | `BAN MEMBERS`
`whitelist` | Manage all whitelists | `whitelist action list [input]` | `wl`, `wordwhitelist`, `rolewhitelist`, `userwhitelist` | - | `BAN MEMBERS`
`tilde` | Toggle tilde~ detection | - | `tl` | - | `BAN MEMBERS`