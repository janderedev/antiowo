import Discord from 'discord.js';
import config from './config';

// Create the sharding manager
const manager = new Discord.ShardingManager('./bot.js', {
    token: config.tokens.discord,
    totalShards: config.shards == 'auto' ? 'auto' : Number(config.shards), // thanks typescript
})

// Spawn the shards
console.log(`Spawning ${config.shards == 'auto' ? 'the recommended amount of' : config.shards} shards`);
manager.spawn();

// Log when a shard is spawned
manager.on('shardCreate', shard => console.log(`Spawned shard ${shard.id}`));
