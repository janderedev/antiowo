// Load dependencies
import Discord from 'discord.js';
import Enmap from 'enmap';
import Log75, { LogLevel } from 'log75';

import getFileList from './library/getFileList';
import BotConfig from './structs/BotConfig';
import Module from './structs/Module';

const logger = new Log75(process.env.NODE_ENV == 'production' ? LogLevel.Standard : LogLevel.Debug);

// Load config
import config from './config';

// Start loading the bot
logger.info(`Starting ${config.name}`);

// Create databases
const db = {
    bot: new Enmap({ name: 'botdata' }),
    guild: new Enmap({ name: 'guilddata', ensureProps: true }),
    user: new Enmap({ name: 'userdata', ensureProps: true }),
    cache: {
        guild: new Map()
    }
}

// Create a Discord client
const bot = new Discord.Client({
    intents: [ // fuck you discord
        'DIRECT_MESSAGES',
        'DIRECT_MESSAGE_REACTIONS',
        'DIRECT_MESSAGE_TYPING',
        'GUILDS',
        'GUILD_BANS',
        'GUILD_EMOJIS_AND_STICKERS',
        'GUILD_INTEGRATIONS',
        'GUILD_INVITES',
        'GUILD_MEMBERS',
        'GUILD_MESSAGES',
        'GUILD_MESSAGE_REACTIONS',
        'GUILD_MESSAGE_TYPING',
        'GUILD_PRESENCES',
        'GUILD_VOICE_STATES',
        'GUILD_WEBHOOKS',
    ]
});

// Create a collection for modules
const modules = new Discord.Collection<string, Module>();

// Export client, databases, modules and config
module.exports = { bot, db, modules, config }

// Mod discord.js and expand db
require('./library/mods')(Discord, db);

// Load functions
module.exports.functions = require('./library/functions')

// Load modules
getFileList('modules').forEach(moduleName => {
    const module = require('./' + moduleName);
    modules.set(module.meta.name, module as Module);
})

// Run modules in order
modules
    .filter(module => module.meta.autorun > 0)
    .sort((a, b) => a.meta.autorun - b.meta.autorun)
    .forEach(module => module.run());