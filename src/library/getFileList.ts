import fs from 'fs';

export default getFileList;

function getFileList(dir: string): string[] {
    let res: string[] = [];

    fs.readdirSync('dist/' + dir).forEach(file => {
        if (!file.endsWith('.d.ts') && !file.endsWith('.map')) {
            const fileDir = `${dir}/${file}`;
            const stat = fs.statSync('dist/' + fileDir);

            if (stat && stat.isDirectory())
                res = [...res, ...getFileList(fileDir)];
            else res.push(fileDir);
        }
    });

    return res;
}