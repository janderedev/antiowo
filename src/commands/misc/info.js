const { bot, functions } = require('../../bot')

exports.run = (m, a) => {
    const embed = functions.embed()
        .setAuthor('Antiowo', bot.user.displayAvatarURL, 'https://antiowo.xyz')
        .setDescription('Antiowo is a discord bot that kicks, bans or otherwise censors anyone who says `owo` or `uwu` in ' +
            'the chat. Antiowo can also detect words that end with a tilde~ and do many other "fun things".\n\n' +
            '**Having issues?**\nMake sure that Antiowo has a higher role than the offender, make sure that Antiowo\'s ' +
            'role can manage messages, ban and kick members. Also make sure the user is not whitelisted in any way. For more support, [join ' +
            'the support server](https://discord.gg/N8Fqcuk).\n\n**Useful links**\nWebsite: https://antiowo.xyz/\nInvite: ' +
            'https://invite.antiowo.xyz/\nSource code: https://gitlab.com/Wait_What_/antiowo\nSupport server: ' +
            'https://discord.gg/N8Fqcuk\n**Vote on top.gg**: https://vote.antiowo.xyz/')
        .setFooter('This bot is meant to be a joke and should not be taken personally')

    m.channel.send({embed})
}

exports.meta = {
    names: ['info'],
    permissions: ['DM'],
    help: {
        description: 'See information about Antiowo',
        usage: '',
        category: 'misc'
    }
}