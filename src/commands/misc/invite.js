const { } = require('../../bot')

exports.run = (m, a) => {
    m.respond('**https://invite.antiowo.xyz**', 'Invite Antiowo')
}

exports.meta = {
    names: ['invite', 'inv', 'link'],
    permissions: ['DM'],
    help: {
        description: 'Invite Antiowo to a server',
        usage: '',
        category: 'misc'
    }
}