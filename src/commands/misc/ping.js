const { bot } = require('../../bot')

exports.run = (m, a) => {
    m.respond(`Pong! **${Math.floor(bot.ping)}** ms :ping_pong:`)
}

exports.meta = {
    names: ['ping', 'pong'],
    permissions: ['DM'],
    help: {
        description: 'See Antiowo\'s ping to Discord',
        usage: '',
        category: '' // Not displayed in help
    }
}