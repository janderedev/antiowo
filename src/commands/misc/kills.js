const { db } = require('../../bot')

exports.run = (m, a) => {
    m.respond(`**${db.bot.get('kills') || 0}** kills (global)`)
}

exports.meta = {
    names: ['kills', 'bans', 'kicks'],
    permissions: ['DM'],
    help: {
        description: 'See Antiowo\'s kill count',
        usage: '',
        category: 'misc'
    }
}