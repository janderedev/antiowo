const { bot, modules, functions } = require('../../bot')
const { help } = modules.get('commandLoader')

exports.run = (m, a) => {
    const prefix = functions.getPrefix(m.guild ? m.guild.id : null)
    const category = (a[0] || '').toLowerCase()

    const embed = functions.embed()

    if (help.has(category)) embed
        .setAuthor(`Help - ${category}`, bot.user.displayAvatarURL)
        .setDescription(
            help.get(category).map(item =>
                `${item.permissions.length > 0 ? `:small_${item.permissions.includes('DM') ?
                'blue' : 'orange'}_diamond:` : ':white_small_square:'} \`${prefix}` +
                `${item.names[0]}\` ${item.description}${item.usage != '' ?
                ` \`${prefix}${item.names[0]} ${item.usage}\`` : ''}`
            ).join('\n')
        )
        .setFooter('Blue - supports DMs | orange - needs permissions')
    
    else {
        const list = Array.from(help.keys())
            .map(category => `:white_small_square: \`${prefix}help ${category}\``)

        embed
            .setAuthor('Help', bot.user.displayAvatarURL)
            .setDescription(`To see commands use:\n${list.join('\n')}`)
    }

    m.channel.send({embed})
}

exports.meta = {
    names: ['help'],
    permissions: ['DM'],
    help: {
        description: 'See all commands and their usage',
        usage: '',
        category: '' // Not displayed in help
    }
}