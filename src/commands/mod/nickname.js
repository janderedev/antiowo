const { db } = require('../../bot')

exports.run = (m, a) => {
    const current = db.guild.get(m.guild.id, 'nicknameScanner')

    m.respond(`:white_check_mark: **${current ? 'Dis' : 'En'}abled** nickname and tag scanning`)

    db.guild.set(m.guild.id, !current, 'nicknameScanner')
}

exports.meta = {
    names: ['nickname', 'nick', 'nk'],
    permissions: ['BAN_MEMBERS'],
    help: {
        description: 'Toggle nickname scanning',
        usage: '',
        category: 'mod'
    }
}