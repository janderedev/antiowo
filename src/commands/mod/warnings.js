const { functions, db } = require('../../bot')
const escape = require('discord.js').Util.escapeMarkdown

exports.run = async (m, a) => {
    const guild = db.guild.get(m.guild.id)

    const displaySyntax = () => m.respond(
        `:white_small_square: Warning count before action: **${guild.warningCount}**\n\n` +
        `**Usage**: \`${functions.getPrefix(m.guild.id)}warnings <@member/number> [clear]\`\n` +
        '**Examples**:\n' +
        `:small_blue_diamond: Set warning amount to 3 - \`${functions.getPrefix(m.guild.id)}warnings 3\`\n` +
        `:small_blue_diamond: See \`user#1234\`'s warnings - \`${functions.getPrefix(m.guild.id)}warnings @user#1234\`\n` +
        `:small_blue_diamond: Reset \`user#1234\`'s warnings - \`${functions.getPrefix(m.guild.id)}warnings @user#1234 clear\``,
    'Warnings')

    if (a.length < 1) return displaySyntax()

    const user = m.mentions.users.first()
    const amount = parseInt(a[0], 10)

    // Set warning count
    if (!isNaN(amount)) {
        db.guild.set(m.guild.id, amount, 'warningCount')
        return m.respond(`:white_check_mark: Set the warning count to **${amount}**`)
    } else

    if (user) {
        // Reset user's warning count
        if ((a[1] || '').match(/clear/i)) {
            db.guild.set(m.guild.id, 0, `warnings.${user.id}`)
            m.respond(`Reset **${escape(user.tag)}**'s warnings to **0**`)
            return
        }

        // Show user's warning count
        const count = guild.warnings[user.id]
        m.respond(`**${escape(user.tag)}** has **${count || 0}** warning${count == 1 ? '' : 's'}`)
        return
    } else return displaySyntax()
}

exports.meta = {
    names: ['warnings', 'warning', 'warn', 'wr'],
    permissions: ['BAN_MEMBERS'],
    help: {
        description: 'Manage warnings',
        usage: '[@member/number] [clear]',
        category: 'mod'
    }
}