const { functions, db } = require('../../bot')
const punishments = [ 'ban', 'kick', 'deletion', 'none', 'random' ]

exports.run = (m, a) => {
    const type = (a[0] || '').match(/^(b|k|d|n|r)/)
    const current = punishments[db.guild.get(m.guild.id, 'punishment')]

    if (!type) return m.respond(
        `:small_blue_diamond: Current punishment: **${current}**\n:small_blue_diamond: Use ` +
        `\`${functions.getPrefix(m.guild.id)}punishment ${punishments.filter(p => p != current).join('/')}\` to ` +
        `change it`, 'Punishment'
    )

    const punishment = punishments.indexOf(punishments.find(p => p[0] == type[0]))

    if (punishment == 4) return m.respond('Random punishments are **coming soon** to Antiowo v5!')

    db.guild.set(m.guild.id, punishment, 'punishment')
    m.respond(`:white_check_mark: Set the punishment to **${punishments[punishment]}**`)
}

exports.meta = {
    names: ['punishment', 'punish', 'pn'],
    permissions: ['BAN_MEMBERS'],
    help: {
        description: 'Manage the punishments',
        usage: '[type]',
        category: 'mod'
    }
}