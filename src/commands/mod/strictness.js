const { functions, db } = require('../../bot')
const strictnesses = [ 'disabled', 'basic', 'regular', 'strict' ]

exports.run = (m, a) => {
    const type = (a[0] || '').match(/^(d|b|r|s)/)
    const current = strictnesses[db.guild.get(m.guild.id, 'strictness')]

    if (!type) return m.respond(
        `:small_blue_diamond: Current strictness: **${current}**\n:small_blue_diamond: Use ` +
        `\`${functions.getPrefix(m.guild.id)}strictness ${strictnesses.filter(p => p != current).join('/')}\` to ` +
        `change it\n\n` +
        ':white_small_square: Disabled: will **not** scan messages for `owo` or `uwu`, unless they are in the ' +
        '**custom blacklist**\n' +
        ':white_small_square: Basic: will only detect **exactly** `owo` or `uwu` (**not** case sensitive)\n' +
        ':white_small_square: Regular: will get triggered by `0ww,u`, but **not** `nOwO` (**has to start** with ' +
        'an `o` or a `u` to prevent **false positives**)\n' +
        ':white_small_square: Strict: will detect **as many forms** of `owo` or `uwu` as **possible**, even if ' +
        'it\'s in a word like `coworker` (apart `antiowo`)', 'Strictness'
    )

    const strictness = strictnesses.indexOf(strictnesses.find(s => s[0] == type[0]))

    db.guild.set(m.guild.id, strictness, 'strictness')
    m.respond(`:white_check_mark: Set the strictness to **${strictnesses[strictness]}**`)
}

exports.meta = {
    names: ['strictness', 'strict', 'st'],
    permissions: ['BAN_MEMBERS'],
    help: {
        description: 'Manage the strictness of owo/uwu detection',
        usage: '[type]',
        category: 'mod'
    }
}