const { db } = require('../../bot')

exports.run = (m, a) => {
    const current = db.guild.get(m.guild.id, 'delete')

    m.respond(`:white_check_mark: **${current ? 'Dis' : 'En'}abled** message deletion after bans of kicks`)

    db.guild.set(m.guild.id, !current, 'delete')
}

exports.meta = {
    names: ['delete', 'deletion', 'dl'],
    permissions: ['BAN_MEMBERS'],
    help: {
        description: 'Toggle message deletion after action',
        usage: '',
        category: 'mod'
    }
}