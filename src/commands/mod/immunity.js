const { db } = require('../../bot')

exports.run = (m, a) => {
    const current = db.guild.get(m.guild.id, 'immunity')

    m.respond(`:white_check_mark: **${current ? 'Dis' : 'En'}abled** Antiowo Passes in this server. Antiowo is ` +
        `${current ? 'sad...' : 'happy!'}\nPlease keep them enabled if you want to support Antiowo, thank you`)

    db.guild.set(m.guild.id, !current, 'immunity')
}

exports.meta = {
    names: ['immunity', 'im'],
    permissions: ['BAN_MEMBERS'],
    help: {
        description: 'Toggle Antiowo Passes',
        usage: '',
        category: 'mod'
    }
}