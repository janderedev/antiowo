const { config, db } = require('../../bot')

exports.run = (m, a) => {
    const prefix = a[0]

    if (prefix && prefix != config.defaultPrefix) {
        db.guild.set(m.guild.id, prefix, 'prefix')
        m.respond(`Set the prefix to \`${prefix}\``)
    } else {
        db.guild.set(m.guild.id, undefined, 'prefix')
        m.respond(`Reset the prefix to \`${config.defaultPrefix}\``)
    }
}

exports.meta = {
    names: ['prefix', 'setprefix', 'pr'],
    permissions: ['MANAGE_MESSAGES'],
    help: {
        description: 'Change the prefix in this server',
        usage: '[prefix]',
        category: 'mod'
    }
}