const { modules } = require('../../bot')

exports.run = async (m, a) => {
    if (!(a[0] || '').match(/^s/i)) {        
        await m.respond('Reloading commands')
        console.log('Reloading commands')
    }

    modules.get('commandLoader').run()
}

exports.meta = {
    names: ['reload', 'rel', 'r'],
    permissions: ['BOT_OWNER'],
    help: {
        description: 'Reload the commands without restarting the bot',
        usage: '',
        category: ''
    }
}