const { bot, db } = require('../../bot')

exports.run = async (m, a) => {
    const user = await (m.mentions.users.first() || bot.fetchUser(a[0]).catch(() => {}))
    if (!user || user.id == m.author.id) return m.respond('Provide a valid user')

    db.ensure.user(user.id)
    const current = db.user.get(user.id, 'blacklist')

    m.respond(`${current ? 'Unb' : 'B'}lacklisted **${user.tag}**`)

    db.user.set(user.id, !current, 'blacklist')
}

exports.meta = {
    names: ['globalblacklist'],
    permissions: ['BOT_OWNER'],
    help: {
        description: 'Blacklist a user',
        usage: '',
        category: ''
    }
}