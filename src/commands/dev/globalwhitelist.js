const { bot, db } = require('../../bot')

exports.run = async (m, a) => {
    const user = await (m.mentions.users.first() || bot.fetchUser(a[0]).catch(() => {}))
    if (!user) return m.respond('Provide a valid user')

    db.ensure.user(user.id)
    const current = db.user.get(user.id, 'whitelist')

    m.respond(`${current ? 'Unw' : 'W'}hitelisted **${user.tag}**`)

    db.user.set(user.id, !current, 'whitelist')
}

exports.meta = {
    names: ['globalwhitelist'],
    permissions: ['BOT_OWNER'],
    help: {
        description: 'Whitelist a user',
        usage: '',
        category: ''
    }
}