const { help } = require('../../modules/core/commandLoader')

exports.run = (m, a) => {
    const output = [
        '# Commands',
        Array.from(help.keys())
            .map(category => [
                `## ${category[0].toUpperCase() + category.slice(1).toLowerCase()}`,
                'Command | Description | Usage | Aliases | DMs | Permissions',
                '--- | --- | --- | --- | --- | ---',
                help.get(category)
                    .map(cmd => [
                        `\`${cmd.names[0]}\``,
                        cmd.description || '-',
                        cmd.usage ? `\`${cmd.names[0]} ${cmd.usage}\`` : '-',
                        cmd.names.slice(1).map(name => `\`${name}\``).join(', ') || '-',
                        cmd.permissions.includes('DM') ? '+' : '-',
                        cmd.permissions.filter(perm => perm != 'DM')
                            .map(name => `\`${name.replace(/_/g, ' ')}\``)
                            .join(', ') || '-'
                    ].join(' | '))
                    .join('\n')
            ].join('\n'))
            .join('\n\n')
    ]

    console.log(output.join('\n'))
    m.respond('Table generated. Check the console')
}
exports.meta = {
    names: ['gentable'],
    permissions: ['BOT_OWNER'],
    help: {
        description: 'Generates command list in markdown',
        usage: '',
        category: ''
    }
}