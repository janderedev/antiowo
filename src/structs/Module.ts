class Module {
    run: Function;
    meta: {
        name: string,
        autorun: number,
    }
}

export default Module;