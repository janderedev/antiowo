class BotConfig {
    tokens: {
        discord: string;
    }
    defaultPrefix: string;
    owners: string[];
    blacklists: {
        disabled: null;
        basic: RegExp;
        regular: RegExp;
        strict: RegExp;
    }
    shards: number | 'auto';
    name: string;
    embedColor: string;
    activity: {
        text: string;
        type: string;
    }
    logCommands: {
        enabled: boolean;
        ignoreBotOwners: boolean;
    }
    logAntiowo: boolean;
    dbDefaults: {
        user: {
            blacklist: boolean;
            whitelist: boolean;

            passes: number;
        },
        guild: {
            prefix: string|undefined;

            punishment: number;
            strictness: number;
            tildeScanner: boolean;
            nicknameScanner: boolean;
            delete: boolean;

            warnings: object;
            warningCount: number;

            wordBlacklist: Array<string>;
            wordWhitelist: Array<string>;
            userWhitelist: Array<string>;
            roleWhitelist: Array<string>;

            immunity: boolean;
            firstRun: boolean;
            ignore: boolean;
        }
    }
}

export default BotConfig;