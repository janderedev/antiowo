const { bot, functions, db, config, modules } = require('../../bot')
const { commands } = require('./commandLoader')
const timeouts = new Set()

exports.run = () => {
    bot.on('message', m => {
        if (m.author.bot) return

        const prefix = functions.getPrefix(m.guild ? m.guild.id : null)

        if (m.content.startsWith(prefix)) {
            db.ensure.user(m.author.id)
            if (db.user.get(m.author.id, 'blacklist')) return

            const args = m.content.split(/\s+/g)
            const commandName = args.shift().slice(prefix.length).toLowerCase()

            if (m.guild && args.slice(args.length - 1, args.length)[0] == '-d') {
                args.pop()
                m.delete().catch(() => {}) 
            }

            if (commands.names.has(commandName)) {
                const command = commands.commands.get(commands.names.get(commandName))

                if (command.meta.permissions.includes('BOT_OWNER')) {
                    if (!config.owners.includes(m.author.id)) return
                } else

                if (m.channel.type == 'dm' && !command.meta.permissions.includes('DM')) {
                    if (timeouts.has(m.author.id)) return
                    
                    m.respond(`Use \`${prefix}help\` to see available commands`, 'You can\'t use this command in DMs')

                    timeouts.add(m.author.id)
                    setTimeout(() => timeouts.delete(m.author.id), 5 * 1000)

                    return 
                } else

                if (m.channel.type == 'text' && !m.member.hasPermission(command.meta.permissions.filter(perm => perm != 'DM'))) {
                    m.respond(`You need ${command.meta.permissions.map(perm => `\`${perm}\``).join(', ')}` +
                        'to run this command')
                        .then(m2 => m2.delete(20 * 1000).catch(() => {}))
                        .catch(() => {})

                    return
                }

                command.run(m, args)

                if (config.logCommands.enabled) {
                    if (config.logCommands.ignoreBotOwners && config.owners.includes(m.author.id)) return

                    console.log(`${m.author.tag} / ${m.author.id} in ${m.guild ? m.guild.name : 'DMs'} used ${m.content}`)
                }
            } else modules.get('scanner').run(m)
        } else {
            modules.get('scanner').run(m)

            if (m.content.startsWith(`<@${bot.user.id}>`) || m.content.startsWith(`<@!${bot.user.id}>`))
                m.respond(`The prefix is \`${prefix}\`. Use \`${prefix}prefix\` to change it`)
        }
    })

    /* bot.on('messageUpdate', (m1, m2) => {
        if (m1.content != m2.content && !m1.author.bot)
            modules.get('scanner').run(m2)
    }) temp. disabled */
}

exports.meta = {
    name: 'messageHandler',
    autorun: 2
}