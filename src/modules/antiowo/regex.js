const { db, functions } = require('../../bot')

exports.run = () => {
    db.guild
        .filter(guild => guild.wordWhitelist.length > 0 || guild.wordBlacklist.length > 0)
        .forEach((g, gid) => functions.updateRegex(gid))
}

exports.meta = {
    name: 'regex',
    autorun: 3
}