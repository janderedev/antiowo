const { bot, db, functions } = require('../../bot')

exports.run = (m) => {
    if (!db.guild.get(m.guild.id, 'firstRun')) return

    const embed = functions.embed()
        .setAuthor('Thanks for inviting Antiowo!', bot.user.displayAvatarURL, 'https://vote.antiowo.xyz')
        .setDescription('You can **blacklist** or **whitelist words**, ' +
            `**roles** and **users**; change the **punishment** to a **ban**/**deletion**; set ` +
            `up a **warning system** and change **strictness** settings. ` +
            `Check out \`${functions.getPrefix(m.guild.id)}help mod\` for more info\n\n` +
            `**Support Antiowo** and get **Antiowo Passes**! \`${functions.getPrefix(m.guild.id)}support\``)
        .setFooter('This message won\'t show up again')

    m.channel.send({embed})
    db.guild.set(m.guild.id, false, 'firstRun')

    console.log(`First run in ${m.guild.name} / ${m.guild.id}`)
}

exports.meta = {
    name: 'firstRun',
    autorun: 0
}