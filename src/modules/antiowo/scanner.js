const { modules, db, config } = require('../../bot')
const folder = require('fold-to-ascii')

const strictnesses = [ 'disabled', 'basic', 'regular', 'strict' ]

exports.run = m => {
    if (!m.guild) return
    db.ensure.user(m.author.id)

    const {
        userWhitelist, roleWhitelist,
        nicknameScanner, tildeScanner,
        strictness, ignore
    } = db.guild.get(m.guild.id)

    const { wordWhitelistRegex, wordBlacklistRegex } = db.cache.guild.get(m.guild.id) || {}

    if (db.user.get(m.author.id, 'whitelist') ||
        userWhitelist.includes(m.author.id) ||
        roleWhitelist.find(id => m.member.roles.has(id)) ||
        ignore || m.author.bot
    ) return

    const match = str => {
        str = folder.foldMaintaining(str)
            .replace(/\n|u wo\w|o wo\w|antiowo/gi, ' ') // Fix false positives for phrases like 'u would' and 'o wow'
            .replace(wordWhitelistRegex || null, '')

        const output = [
            ...(str.match(config.blacklists[strictnesses[strictness]]) || []),
            ...(str.match(wordBlacklistRegex || null) || []),
            ...(tildeScanner ? (str.match(/\S+~(\B|$)/g) || []) : [])
        ].filter((e, i, s) => i == s.indexOf(e))

        return output.length > 0 ? output : null
    }

    const matches = {
        message: match(m.content),
        nickname: nicknameScanner ? match(m.member ? m.member.displayName : '') : null,
        tag: nicknameScanner ? match(m.author.tag) : null
    }

    if (matches.message || matches.nickname || matches.tag)
        modules.get('executor').run(m, matches)
}

exports.meta = {
    name: 'scanner',
    autorun: 0
}